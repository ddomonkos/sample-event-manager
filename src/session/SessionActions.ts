import {Actions} from '../core/Actions';
import {FluxDispatcher} from '../core/FluxDispatcher';
import {ActionTypes} from './ActionTypes';
import {User} from './SessionStore';

export class SessionActions extends Actions {

  constructor(
    Dispatcher: FluxDispatcher
  ) {
    'ngInject';
    super(Dispatcher);
  }

  setUser(user: User) {
    this.dispatch(ActionTypes.SET_USER, user);
  }
}
