import {EventEmitter} from 'eventemitter3';
import {FluxDispatcher, IAction} from '../core/FluxDispatcher';
import {ActionTypes} from './ActionTypes';

export interface User {
  uuid: string;
  name?: string;
}

export class SessionStore extends EventEmitter {

  private user: User;

  constructor(
    private Dispatcher: FluxDispatcher
  ) {
    'ngInject';
    super();

    this.Dispatcher.register((action: IAction) => {
      switch (action.type) {
        case ActionTypes.SET_USER:
          this.setUser(<User> action.payload);
          break;
      }
    });
  }

  private setUser(user: User) {
    this.user = user;
    this.emit('change');
  }

  getUser(): User {
    return this.user;
  }
}
