import {applyNamespace} from '../core/ActionHelpers';

export const ActionTypes = {
    SET_USER: 'SET_USER'
};

applyNamespace('session', ActionTypes);
