import {IModule} from 'angular';
import {SessionActions} from './SessionActions';
import {SessionStore} from './SessionStore';

export function bindSession(module: IModule) {
    module
        .service('SessionActions', SessionActions)
        .service('SessionStore', SessionStore);
}
