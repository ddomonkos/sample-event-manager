import {SessionStore} from '../../session/SessionStore';
import {EventsActions} from '../EventsActions';
import {Event, EventsStore} from '../EventsStore';
const template = require('./template.html');
import * as moment from 'moment-timezone';

export const EventDetails = {
    template,

    controller: class Ctrl {
        event: Event;

        constructor(
            private EventsStore: EventsStore,
            private EventsActions: EventsActions,
            private SessionStore: SessionStore
        ) {
            'ngInject';
            this.reload();
            EventsStore.on('change', this.reload.bind(this));
        }

        reload() {
            this.event = this.EventsStore.getSelected();
        }

        delete() {
            const confirmed = confirm('The event will be deleted.');
            if (confirmed) {
                this.EventsActions.delete(this.event);
            }
        }

        duplicate() {
            this.EventsActions.duplicate(this.event);
        }

        formatDate(date) {
            if (date) {
                const mDate = moment.tz(date, this.event.timezone);
                return mDate.format('MMM D Y - h a');
            }
        }

        isOngoing(): boolean {
            return moment().isSameOrAfter(this.event.starts_at) && moment().isSameOrBefore(this.event.ends_at);
        }

        canDelete() {
            return moment().isBefore(this.event.starts_at) &&
                this.event.owner.uuid === this.SessionStore.getUser().uuid;
        }

        canDuplicate() {
            return moment().isBefore(this.event.starts_at);
        }
    }
}