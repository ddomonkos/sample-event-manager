import {IModule} from 'angular';
import {EventDetails} from './EventDetails';
import './styles.scss';

export function bindEventDetails(module: IModule) {
    module.component('eventDetails', EventDetails);
}
