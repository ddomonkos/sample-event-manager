import {IModule} from 'angular';
import {bindEventDetails} from './EventDetails';
import {EventsActions} from './EventsActions';
import {bindEventsList} from './EventsList';
import {EventsStore} from './EventsStore';
import './styles.scss';

export function bindEvents(module: IModule) {
    module
        .service('EventsActions', EventsActions)
        .service('EventsStore', EventsStore);

    bindEventDetails(module);
    bindEventsList(module);
}
