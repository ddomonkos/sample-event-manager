import {Event, EventsStore} from '../EventsStore';
const template = require('./template.html');
import * as _ from 'lodash';
import * as moment from 'moment-timezone';
import {SessionStore} from '../../session/SessionStore';
import {EventsActions} from '../EventsActions';

export const EventsList = {
    template,

    controller: class Ctrl {
        events: Event[];
        selected: Event;
        nameInputActivated = false;
        newEventName = '';
        searchQuery = '';
        ownEventsOnly = false;

        constructor(
            private EventsStore: EventsStore,
            private EventsActions: EventsActions,
            private SessionStore: SessionStore
        ) {
            'ngInject';
            this.reload();
            EventsStore.on('change', this.reload.bind(this));
        }

        reload() {
            const includes = (str) => {
                const query = this.searchQuery;
                const regex = new RegExp(`(^|\s)${_.escapeRegExp(query)}`, 'gi');
                return str.search(regex) >= 0;
            }

            let events;
            if (this.searchQuery.length > 0) {
                events = this.EventsStore.getEvents().filter(event =>
                    includes(event.name) || includes(event.description) || includes(event.location)
                );
            } else {
                events = this.EventsStore.getEvents();
            }

            if (this.ownEventsOnly) {
                const currentUser = this.SessionStore.getUser();
                events = events.filter(event => event.owner.uuid === currentUser.uuid);
            }

            this.events = events;
            this.selected = this.EventsStore.getSelected();
        }

        handleSelect(event: Event) {
            this.EventsActions.select(event);
        }

        isSelected(event: Event): boolean {
            return this.selected && this.selected.uuid === event.uuid;
        }

        isOngoing(event: Event): boolean {
            return moment().isSameOrAfter(event.starts_at) && moment().isSameOrBefore(event.ends_at);
        }

        activateNameInput() {
            this.nameInputActivated = true;
        }

        createEvent() {
            this.EventsActions.create({name: this.newEventName});
            this.nameInputActivated = false;
            this.newEventName = '';
        }

        formatDate(date, timezone) {
            if (date) {
                const mDate = moment.tz(date, timezone);
                return mDate.format('MMM D');
            }
        }

        get futureEvents() {
            return this.events
                .filter(event => moment().isSameOrBefore(event.ends_at))
                .sort((a, b) => moment(a.starts_at).diff(b.starts_at));
        }
        
        get pastEvents() {
            return this.events
                .filter(event => moment().isAfter(event.ends_at))
                .sort((a, b) => moment(b.starts_at).diff(a.starts_at));
        }
    }
}