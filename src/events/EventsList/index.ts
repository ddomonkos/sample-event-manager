import {IModule} from 'angular';
import {EventsList} from './EventsList';
import './styles.scss';

export function bindEventsList(module: IModule) {
    module.component('eventsList', EventsList);
}
