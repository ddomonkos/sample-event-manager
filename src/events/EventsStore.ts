import {EventEmitter} from 'eventemitter3';
import * as _ from 'lodash';
import * as moment from 'moment';
import {FluxDispatcher, IAction} from '../core/FluxDispatcher';
import {SessionStore, User} from '../session/SessionStore';
import {ActionTypes} from './ActionTypes';

export interface Event {
  uuid?: string;
  name?: string;
  description?: string;
  location?: string;
  starts_at?: string;
  ends_at?: string;
  [propName: string]: any;
}

export class EventsStore extends EventEmitter {

  private events: Event[];
  private selectedUuid: string;

  constructor(
    private Dispatcher: FluxDispatcher,
    private SessionStore: SessionStore
  ) {
    'ngInject';
    super();

    this.events = [];

    this.Dispatcher.register((action: IAction) => {
      switch (action.type) {
        case ActionTypes.LOAD:
          this.load(<Event[]> action.payload);
          break;

        case ActionTypes.SELECT:
          this.select(<string> action.payload);
          break;

        case ActionTypes.CREATE:
          this.create(<Event> action.payload);
          break;

        case ActionTypes.DELETE:
          this.delete(<string> action.payload);
          break;

        case ActionTypes.DUPLICATE:
          this.duplicate(<string> action.payload);
          break;
      }
    });
  }

  private load(events: Event[]) {
    this.events = this.events.concat(events);
    this.emit('change');
  }

  private select(uuid: string) {
    this.selectedUuid = uuid;
    this.emit('change');
  }

  private create(event: Event) {
    event = _.cloneDeep(event);
    this.generateUuid(event);
    this.setOwner(event, this.SessionStore.getUser());
    event.location = 'Bratislava, Slovakia';
    event.description = 'Something';
    event.starts_at = moment().add(1, 'day').utc().format();
    event.ends_at = moment().add(2, 'days').utc().format();
    this.events = this.events.concat([event]);
    this.selectedUuid = event.uuid;
    this.emit('change');
  }

  private delete(uuid: string) {
    const event = this.find(uuid);
    if (event && this.isOwner(event)) {
      this.events = this.events.slice();
      this.events.splice(this.events.indexOf(event), 1);
      this.emit('change');
    }
  }

  private duplicate(uuid: string) {
    const event = this.find(uuid);
    if (event) {
      const newEvent = _.cloneDeep(event);
      this.generateUuid(newEvent);
      this.setOwner(newEvent, this.SessionStore.getUser());
      this.events = this.events.concat([newEvent]);
      this.selectedUuid = newEvent.uuid;
      this.emit('change');
    }
  }

  getEvents(): Event[] {
    return this.events;
  }

  getSelected(): Event {
    return this.find(this.selectedUuid);
  }

  private find(uuid: string) {
    return this.events.find(event => event.uuid === uuid);
  }

  private isOwner(event: Event): boolean {
    if (event) {
      const user = this.SessionStore.getUser();
      return event.owner.uuid === user.uuid;
    }
  }

  private setOwner(event: Event, owner: User) {
    event.owner = _.pick(owner, ['uuid', 'name']);
  }

  private generateUuid(event: Event) {
    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      const r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    event.uuid = uuid;
  }
}
