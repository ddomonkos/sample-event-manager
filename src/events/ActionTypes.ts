import {applyNamespace} from '../core/ActionHelpers';

export const ActionTypes = {
    CREATE: 'CREATE',
    DELETE: 'DELETE',
    DUPLICATE: 'DUPLICATE',
    LOAD: 'LOAD',
    SELECT: 'SELECT'
};

applyNamespace('events', ActionTypes);
