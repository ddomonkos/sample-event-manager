import {Actions} from '../core/Actions';
import {FluxDispatcher} from '../core/FluxDispatcher';
import {ActionTypes} from './ActionTypes';
import {Event} from './EventsStore';

export class EventsActions extends Actions {

  constructor(
    Dispatcher: FluxDispatcher
  ) {
    'ngInject';
    super(Dispatcher);
  }

  load(events: Event[]) {
    this.dispatch(ActionTypes.LOAD, events);
  }

  select(event: Event) {
    this.dispatch(ActionTypes.SELECT, event.uuid);
  }

  create(event: Event) {
    this.dispatch(ActionTypes.CREATE, event);
  }

  delete(event: Event) {
    this.dispatch(ActionTypes.DELETE, event.uuid);
  }

  duplicate(event: Event) {
    this.dispatch(ActionTypes.DUPLICATE, event.uuid);
  }
}
