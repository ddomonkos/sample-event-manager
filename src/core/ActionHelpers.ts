export function applyNamespace(namespace: string, types: Object) {
    Object
        .keys(types)
        .forEach((type) => types[type] = `${namespace}/${types[type]}`);
}
