import {FluxDispatcher} from './FluxDispatcher';

export class Actions {
    constructor(
        protected Dispatcher: FluxDispatcher
    ) {
        'ngInject';
    }

    protected dispatch(type: string, payload: any) {
        this.Dispatcher.dispatch({type, payload});
    }
}
