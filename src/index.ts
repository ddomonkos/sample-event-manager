import {module} from 'angular';
import 'normalize.css';
import './base.scss';
import {FluxDispatcher} from './core/FluxDispatcher';
import {bindEvents} from './events';
import {EventsActions} from './events/EventsActions';
import {Event, EventsStore} from './events/EventsStore';
import {bindSession} from './session';
import {SessionActions} from './session/SessionActions';
import {SessionStore, User} from './session/SessionStore';

const app = module('sample-event-manager', [])
    .service('Dispatcher', FluxDispatcher);

bindEvents(app);
bindSession(app);


app.run(
    (
        EventsActions: EventsActions,
        SessionActions: SessionActions,

        // Eager load the services, otherwise they don't listen yet
        EventsStore: EventsStore,
        SessionStore: SessionStore
    ) => {
        'ngInject';

        const data = require('@resources/data.json');
        EventsActions.load(<Event[]> data.events);
        SessionActions.setUser(<User> data.current_user);
    }
);