const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

function resolve(relativePath) {
    return path.resolve(__dirname, relativePath);
}

module.exports = {
    entry: "./src/index.ts",

    output: {
        filename: "js/bundle.js",
        path: resolve("dist")
    },

    devtool: 'cheap-module-source-map',
    // devtool: "source-map",

    resolve: {
        modules: ["node_modules"],
        extensions: [".ts", ".js", ".json"],
        alias: {
            "@resources": resolve("resources")
        }
    },

    module: {
        rules: [
            { test: /\.ts$/, loader: "awesome-typescript-loader" },

            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },

            { test: /\.html$/, loader: 'html-loader' },

            { test: /\.css$/, use: [
                'style-loader',
                {
                    loader: 'css-loader',
                    options: {
                        importLoaders: 1
                    }
                },
                {
                    loader: 'postcss-loader',
                    options: {
                        plugins: function () {
                            return [require('autoprefixer')];
                        }
                    }
                }
            ]},

            { test: /\.scss$/, use: [
                'style-loader',
                {
                    loader: 'css-loader',
                    options: {
                        importLoaders: 1
                    }
                },
                {
                    loader: 'postcss-loader',
                    options: {
                        plugins: function () {
                            return [require('autoprefixer')];
                        }
                    }
                },
                'sass-loader'
            ]}
        ]
    },
    
    plugins: [
        new HtmlWebpackPlugin({
            inject: true,
            template: resolve("public/index.html"),
        })
    ]
};